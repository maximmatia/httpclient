<?php

namespace HttpClient;

use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\ResponseInterface;

class Response
{
    protected ResponseInterface $response;
    protected ?string           $baseUri;

    /**
     * @return ResponseInterface
     */
    public function getParentResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @return bool
     */
    public function isJson(): bool
    {
        $body = $this->getBody();
        if (in_array($body[0], ['[', '{'])) {
            json_decode($body);

            return (json_last_error() == JSON_ERROR_NONE);
        } else {
            return false;
        }
    }

    /**
     * @param bool $assoc
     * @return mixed
     */
    public function parseJson(bool $assoc = false)
    {
        return json_decode($this->getBody(), $assoc);
    }

    /**
     * Получение raw тела ответа
     *
     * @return string
     */
    public function getBody(): string
    {
        return (string) $this->getParentResponse()->getBody();
    }

    /**
     * Получение редеректа
     *
     * @return null|string
     */
    public function getRedirect(): ?string
    {
        $redirect = $this->getParentResponse()->getHeaderLine('Location');
        if ($redirect) {
            if (strpos($redirect, 'http') !== 0) {
                if (!is_null($this->baseUri)) {
                    $parsedUrl = parse_url($this->baseUri);
                    $redirect  = sprintf(
                        '%s://%s%s%s',
                        $parsedUrl['scheme'],
                        $parsedUrl['host'],
                        strpos($redirect, '/') !== 0 ? '/' : '',
                        $redirect
                    );
                }
            }

            return $redirect;
        } else {
            return null;
        }
    }

    /**
     * @param $from
     * @param $to
     * @return $this
     */
    public function convertEncoding($from, $to): Response
    {
        $body           = Utils::streamFor(mb_convert_encoding($this->getBody(), $to, $from));
        $this->response = $this->response->withBody($body);

        return $this;
    }

    /**
     * @param ResponseInterface $response
     * @param string|null $baseUri
     * @return $this
     */
    public function __invoke(ResponseInterface $response, string $baseUri = null): Response
    {
        $this->baseUri  = $baseUri;
        $this->response = $response;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->response, $name)) {
            return call_user_func_array([$this->response, $name], $arguments);
        }

        trigger_error('Call to undefined method ' . __CLASS__ . '::' . $name . '()', E_USER_ERROR);
    }

    /**
     * @param string $body
     * @param string|null $baseUrl
     * @return self
     */
    public static function create(string $body, string $baseUrl = null): self
    {
        $response = new static();

        return $response(Message::parseResponse($body), $baseUrl);
    }

    /**
     * @return self
     */
    public static function createEmptyResponse(): self
    {
        $response = new static();

        return $response(new GuzzleResponse(200));
    }
}
